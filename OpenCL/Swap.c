#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <GLUT/glut.h>
#include <OpenGL/OpenGL.h>
#include <OpenCL/opencl.h>
#include <sys/stat.h>

// Include the automatically-geneated header which provides the
// kernel block declaration.
#include "kernel.cl.h"
#include "stat_kernel.cl.h"

#define ARRAY_SIZE 1024*768

// Initial size of graphics window on your screen.
const int WIDTH  = 1024; // in pixels
const int HEIGHT = 768; //
// Current size of window (will change when you resize window)
int width  = WIDTH;
int height = HEIGHT;
int start = 0;

GLenum flag = GL_POINTS;

const int g_cancerCellsRate = 50;

int g_iteration_counter = 0;

double g_wait_time = 0;

bool g_flag = false;
cl_int counter=0;

static cl_context contextGPU;
static cl_context contextCPU;


static dispatch_queue_t queue;
static dispatch_queue_t queueCPU;
static dispatch_semaphore_t cl_gl_semaphore;
static void *device_input;
static void *device_CPU_input;
static void *device_results;

static cl_int* host_input;

//function used to print the used devices in the program
//param1: device id of working device
static void display_device(cl_device_id device){
    char name_buf[128];
    char vendor_buf[128];
    
    clGetDeviceInfo(
                    device, CL_DEVICE_NAME, sizeof(char)*128, name_buf, NULL);
    clGetDeviceInfo(
                    device, CL_DEVICE_VENDOR, sizeof(char)*128, vendor_buf, NULL);
    
    fprintf(stdout, "Using OpenCL device: %s %s\n", vendor_buf, name_buf);
}

//function that is responsable for correct program exit
static void shutdown_opencl(void){
    gcl_free(device_input);
    gcl_free(device_results);
    free(host_input);
    
    clReleaseContext(contextGPU);
    
    dispatch_release(cl_gl_semaphore);
    dispatch_release(queue);
    dispatch_release(queueCPU);

}

//function used to instantiate the device context and to queue them
static int setup_compute_devices(){
    
    // Ask for the global OpenCL context:
    // Note: If you will not be enqueing to a specific device, you do not need
    // to retrieve the context.
    
    contextGPU = gcl_get_context();
    contextCPU = gcl_get_context();
    
    // Query this context to see what kinds of devices are available.
    size_t length;
    cl_device_id devices[3];
    clGetContextInfo(contextGPU, CL_CONTEXT_DEVICES, sizeof(devices), devices, &length);
    
   
    // Create a dispatch queue.
    queue = gcl_create_dispatch_queue(CL_DEVICE_TYPE_USE_ID, devices[1]);
    if (!queue){
        fprintf(stdout, "Unable to create a GPU-based dispatch queue.\n");
        exit(1);
    }
    
    clGetContextInfo(contextCPU, CL_CONTEXT_DEVICES, sizeof(devices), devices, &length);
    
    // Create a dispatch queue.
    queueCPU = gcl_create_dispatch_queue(CL_DEVICE_TYPE_USE_ID, devices[2]);
    if (!queue){
        fprintf(stdout, "Unable to create a CPU-based dispatch queue.\n");
        exit(1);
    }

    // Display the OpenCL device associated with this dispatch queue.
    display_device(gcl_get_device_id_with_dispatch_queue(queue));
    display_device(gcl_get_device_id_with_dispatch_queue(queueCPU));
    
    return CL_SUCCESS;
}

    
//function responsable for memory allocation on all devices and creation of the main array
static int setup_compute_memory(){
    unsigned int i;
    
    // Create some input data on the _host_ ...
    host_input = (int*)malloc(sizeof(cl_int) * ARRAY_SIZE);
    
    // ... and fill it with regular cells.
    for (i=0; i<ARRAY_SIZE; i++)
        host_input[i] = 0;
    
    //preparing the field...
    int _cancer = ARRAY_SIZE * g_cancerCellsRate/100;
    
    //...random distribution of cancer cells
    for (i = 0; i!= _cancer; ++i){
        int _temp = rand()%(ARRAY_SIZE);
        while(host_input[_temp] == 1){
            _temp = rand()%(ARRAY_SIZE);
        }
        host_input[_temp] = 1;
    }
    
    
    // First, allocate some memory on our OpenCL device to hold the input.
    
    // Memory allocation 1: Create a buffer big enough to hold the input.
    //and pass the host-side input data.  This instructs OpenCL to initialize the
    // device-side memory region with the supplied host data.

    device_input = gcl_malloc(sizeof(cl_int)*ARRAY_SIZE, host_input, CL_MEM_COPY_HOST_PTR);
    
    // Memory allocation 2: Create a buffer to store the results
    // of our kernel computation.
    device_results = gcl_malloc(sizeof(cl_int)*ARRAY_SIZE, host_input, CL_MEM_COPY_HOST_PTR);
    
    //Memory allocation 3: Create a buffer for CPU device and initialize the device-side memory region with the supplied host data.
    device_CPU_input = gcl_malloc(sizeof(cl_int)*ARRAY_SIZE, host_input, CL_MEM_COPY_HOST_PTR);
    
    return CL_SUCCESS;
}

//function responsable for calling GPU kernel to run it on GPU device
static int recompute(void){
    
    dispatch_sync(queue, ^{
        //asking OpenCL the optimal work size according to our kernel
        size_t wgs;
        gcl_get_kernel_block_workgroup_info(iterating_kernel, CL_KERNEL_WORK_GROUP_SIZE, sizeof(wgs), &wgs, NULL);
        
        cl_ndrange range = {
            1,          // We're using a 1-dimensional execution.
            {0, 0, 0},        // Start at the beginning of the range.
            {ARRAY_SIZE, 0, 0},    // Execute 'COUNT' work items.
            {wgs, 0, 0}         // passing the optimal work size
            // into workgroups.
        };
        
        //calling the kernel
        iterating_kernel(
                         &range, (cl_int*) device_input,
                         (cl_int*) device_results, start );
        
        //storing the kernel result back to input array for further itereations
        gcl_memcpy(device_input, device_results, sizeof(cl_int)*ARRAY_SIZE);
        
        //returning zero value to medicine point of ionjection
        start = 0;
        
        //transmitting back to main the kernl results
        gcl_memcpy(host_input, device_results, sizeof(cl_int)*ARRAY_SIZE);
        
        //also copying the result into CPU device memory
        device_CPU_input = gcl_malloc(sizeof(cl_int)*ARRAY_SIZE, host_input, CL_MEM_COPY_HOST_PTR);

    });
    
    return 0;
    
}
//function responsable of running the kernel on CPU device
static int initCPUKernel(){
    
        // Create a dispatch semaphore.
        cl_gl_semaphore = dispatch_semaphore_create(0);
        if (!cl_gl_semaphore){
            printf("Error:Failed to create a dispatch semaphore!\n");
            return EXIT_FAILURE;
        }
    
    
    dispatch_sync(queue, ^{
        //similary to GPU case leaving the GPU to find for us an optimal size
        size_t wgs;
        gcl_get_kernel_block_workgroup_info(statistics_kernel, CL_KERNEL_WORK_GROUP_SIZE, sizeof(wgs), &wgs, NULL);
    
        
        cl_ndrange range = {
            1,          // We're using a 1-dimensional execution.
            {0, 0, 0},        // Start at the beginning of the range.
            {ARRAY_SIZE, 0, 0},
            {wgs, 0, 0}
        };
        
        //running CPU kernel
        statistics_kernel(&range, (cl_int*) device_CPU_input, ARRAY_SIZE);
        
    });
    
    
     dispatch_semaphore_signal(cl_gl_semaphore);
    
    return 0;
    
}

//setup function for openCL initialization both the compute devices and their memories
static int setup_opencl(){
    
    int err;
    err = setup_compute_devices();
    if (err != CL_SUCCESS)
        return err;
    
    err = setup_compute_memory();
    if (err != CL_SUCCESS)
        return err;
    
    return CL_SUCCESS;
}

//setup function for GPU initialization
int setup_opengl(void){
    
    glMatrixMode(GL_PROJECTION);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    GLfloat aspect = (GLfloat) width / height;
    gluPerspective(45, aspect, 0.1f, 10.0f);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    
    return GL_NO_ERROR;
}

//initialization of OpenGL and OpenCL
int init(){
    int err;
    
    err = setup_opengl();
    if (err != GL_NO_ERROR)
    {
        printf ("Failed to setup OpenGL state!");
        exit (err);
    }
    
    err = setup_opencl();
    if (err != GL_NO_ERROR)
    {
        printf ("Failed to setup OpenCL state! Error %d\n", err);
        exit (err);
    }
    
    return CL_SUCCESS;
}

/**
 Desc : openGl part responsable to display the grid and running the kernel calculation at each iteration of glutMainLoop();
 */

void display(){
    glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT*/);
    
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    
    //++++++++++++++
    if(g_flag == false){

    int err = recompute();
    if (err != 0)
    {
        printf("error %d from recompute!\n", err);
        shutdown_opencl();
        exit(1);
    }
    
            err = initCPUKernel();
        if (err != 0){
            printf("error %d from initCPUKernel!\n", err);
            shutdown_opencl();
            exit(1);
        }
    }
    //++++++++++++++++
    
    
    gluOrtho2D(0, width, height, 0);
    
    glClearColor(1, 1, 1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    
    glBegin(flag); //points/quads
    for(int x = 0; x < 1024; x++)
    {
        for(int y = 0; y < 768; y++)
        {
            if (host_input[y*1024+x]==0){
                glColor3f(0, 1, 0);
                glVertex2f(x, y);
                
                if(flag== GL_QUADS) {
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                }
            }
            
            if (host_input[y*1024+x]==1){
                glColor3f(1, 0, 0);
                glVertex2f(x, y);
                if(flag== GL_QUADS) {
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                }
            }
            
            if (host_input[y*1024+x]==10) {
                glColor3f(1, 1, 0);
                glVertex2f(x, y);
                if(flag== GL_QUADS) {
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                }
            }
        }
    }
    sleep(g_wait_time);
    glEnd();
    
    glutSwapBuffers();
}

/**
 Desc : IO call for 3 cases: exit, zoom, and insert medicines
 @param1: key of pressed button
 @param: x- mouse position
 @param: y- mouse position
 */

void keyboard ( unsigned char key, int mousePositionX, int mousePositionY){
    switch ( key )
    {
        //- TAB - insert the medicine cells
        case 9:
        //assigning the thread to medicine injection
        g_flag = true;
        int _x, _y;
        printf("\nInsert the point of injection separated by space \n");
        scanf("%d", &_x);
        scanf("%d", &_y);
        start = _y*1024+_x;
        g_flag = false;

        break;
        
        //-ESC- exit the program
        case 27:
        shutdown_opencl();
        exit ( 0 );
        
        break;
        
        //- space - zoom in 6 -times
        case 32:
        flag = GL_QUADS;
        width = width/6;
        height = height/6;
        break;
        
        //- + - zoom to original size
        case 43:
        flag = GL_POINTS;
        width =WIDTH;
        height = HEIGHT;
        break;
    }
    
}


int main (int argc, char ** argv){
    
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("Cells' grid");
    
    if (init() == GL_NO_ERROR){

        glutDisplayFunc(display);
        glutKeyboardFunc(keyboard);
        glutIdleFunc(display);
        glutMainLoop();
    }
}

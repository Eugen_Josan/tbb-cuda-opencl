// medicines injection method
//param1 : g_pixels - main array of cells
//param2: g_pixels2 - secondary array for storing cells
//param3: incerting point of medicines

void medicines( global int* g_pixels, global int* g_pixels2, int start)
{
    // We've launched our kernel (in the host-side code) such that each
    // work item squares one incoming float.  The item each work item
    // should process corresponds to its global work item id.
    
    
    size_t i = get_global_id(0);
    
    if(start != 0){
        g_pixels2[start - 1024 - 1] = 10;
        g_pixels2[start - 1024] = 10;
        g_pixels2[start - 1024 + 1] = 10;
        g_pixels2[start - 1] = 10;
        g_pixels2[start + 1] = 10;
        g_pixels2[start + 1024 - 1] = 10;
        g_pixels2[start + 1024] = 10;
        g_pixels2[start + 1024 + 1] = 10;
        g_pixels2[start] = 10;
    }
    
    int surroundingCells=0;
    
    
    if(i > 1023 && i < 1024*767
       && (i % 1024) != 0 && (i % 1023) != 0){
        
        surroundingCells = g_pixels[i - 1024 - 1] +
        g_pixels[i - 1024] +
        g_pixels[i - 1024 + 1] +
        g_pixels[i - 1] +
        g_pixels[i + 1] +
        g_pixels[i + 1024 - 1] +
        g_pixels[i + 1024] +
        g_pixels[i + 1024 + 1];
        
    }else{
        if (i > 0 && i < 1023){
            surroundingCells =
            g_pixels[i - 1] +
            g_pixels[i + 1] +
            g_pixels[i + 1024 - 1] +
            g_pixels[i + 1024] +
            g_pixels[i + 1024 + 1];
        }
        if ((i % 1024) == 0 && i <  1024*767 && i > 0){
            surroundingCells =
            g_pixels[i - 1024] +
            g_pixels[i - 1024 + 1] +
            g_pixels[i + 1] +
            g_pixels[i + 1024] +
            g_pixels[i + 1024 + 1];
        }
        if ((i % 1023) == 0 && i < 1024*768-1 && i > 1023){
            surroundingCells =
            g_pixels[i - 1024 - 1] +
            g_pixels[i - 1024] +
            g_pixels[i - 1] +
            g_pixels[i + 1024 - 1] +
            g_pixels[i + 1024];
        }
        if (i > 1024*767 && i < 1024*768-1){
            surroundingCells =
            g_pixels2[i - 1] +
            g_pixels2[i + 1] +
            g_pixels2[i - 1024 - 1] +
            g_pixels2[i - 1024] +
            g_pixels2[i - 1024 + 1];
        }
        if(i == 0){
            surroundingCells =
            g_pixels[i - 1024] +
            g_pixels[i - 1024 + 1] +
            g_pixels[i + 1];
        }
        if(i == 1023){
            surroundingCells =
            g_pixels[i - 1] +
            g_pixels[i + 1024 - 1] +
            g_pixels[i + 1024];
        }
        if(i ==  1024*767){
            surroundingCells =
            g_pixels[i - 1024] +
            g_pixels[i - 1024 + 1] +
            g_pixels[i + 1];
        }
        if(i == 1024*768-1){
            surroundingCells =
            g_pixels[i - 1024 - 1] +
            g_pixels[i - 1024] +
            g_pixels[i - 1];
        }
    }
    
    if(g_pixels[i] == 0 && surroundingCells >= 10){
        g_pixels2[i] = 10;
    }
    
    
    
}



//kernel method for cells dynamics
//param1 : g_pixels - main array of cells
//param2: g_pixels2 - secondary array for storing cells
//param3: incerting point of medicines for medicine method

__kernel void iterating( global int* g_pixels, global int* g_pixels2, int start )
{
    // We've launched our kernel (in the host-side code) such that each
    // work item squares one incoming int.  The item each work item
    // should process corresponds to its global work item id.
    
    
    size_t i = get_global_id(0);
    
    
    if((i >= 1025)&&(i <= 1024*768-1025)){

    
    int surroundingCells=0;

    
    surroundingCells = g_pixels[i - 1024 - 1] +
    g_pixels[i - 1024] +
    g_pixels[i - 1024 + 1] +
    g_pixels[i - 1] +
    g_pixels[i + 1] +
    g_pixels[i + 1024 - 1] +
    g_pixels[i + 1024] +
    g_pixels[i + 1024 + 1];
    
        //checking and if it is the case modify the cancer cell
        //it is cured into a healthy cell and all the surrounding medicine cells also
        // become healthy cells
        if (g_pixels[i] == 1) {
            if ((surroundingCells / 10) >= 6){
                g_pixels2[i] = 10;
//                if (g_pixels[i - 1024 - 1] == 10) g_pixels2[i - 1024 - 1] = 0;
//                if (g_pixels[i - 1024] == 10) g_pixels2[i - 1024] = 0;
//                if (g_pixels[i - 1024 + 1] == 10) g_pixels2[i - 1024 + 1] = 0;
//                if (g_pixels[i - 1] == 10) g_pixels2[i - 1] = 0;
//                if (g_pixels[i + 1] == 10) g_pixels2[i + 1] = 0;
//                if (g_pixels[i + 1024 - 1] == 10) g_pixels2[i + 1024 - 1] = 0;
//                if (g_pixels[i + 1024] == 10) g_pixels2[i + 1024] = 0;
//                if (g_pixels[i + 1024 + 1] == 10) g_pixels2[i + 1024 + 1] = 0;
                
            }
        }
        
        // if not cancer cell then it is normal or medicine cell
        if (g_pixels[i] != 1) {
            if ((surroundingCells % 10) >= 6) {
                g_pixels2[i] = 1;
            }
        }
    }
    medicines(g_pixels, g_pixels2, start);
}
//kernel that will be runned on CPU for dispatching the statistical data
//param1: h_A1 - main array of cells
//param2: numElements - number of cells
__kernel void statistics( global int* h_A1, int numElements)
{
    
    int normal = 0;
    int cancer = 0;
    int medicines = 0;

    size_t i = get_global_id(0);

    if(0<i<numElements){
    
        if(h_A1[i] == 0) ++normal;
        if(h_A1[i] == 1) ++cancer;
        if(h_A1[i] == 10) ++medicines;
    
    barrier(CLK_LOCAL_MEM_FENCE);
        

    if(i == 0){
        for(int j = 0; j != numElements; ++j){
        if(h_A1[j] == 0) ++normal;
        if(h_A1[j] == 1) ++cancer;
        if(h_A1[j] == 10) ++medicines;
        }
        
        printf("====================================================\nTotal number of cancer cells is %d\nTotal number of normal cells is %d\nTotal number of medicine cells is %d\n====================================================",cancer, normal, medicines);
    }
    }
}

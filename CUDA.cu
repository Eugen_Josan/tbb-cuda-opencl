#include <iostream>
#include <string>

#include <stdio.h>

#include <stdlib.h>

#include <GLUT/glut.h>
#include <unistd.h>
#include <OpenGL/OpenGL.h>

#include <math.h>
#include <iomanip>
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>



#include <vector>

using namespace std;

#define BLOCK_SIZE 16

// Initial size of graphics window on your screen.
const int WIDTH  = 1024; // in pixels
const int HEIGHT = 768; //


// Current size of window (will change when you resize window)
int width  = WIDTH;
int height = HEIGHT;

GLenum flag = GL_POINTS;

const int g_cancerCellsRate = 50;

int g_iteration_counter = 0;

double g_wait_time = 0;

bool g_flag = false;

int numElements = 1024*768;
size_t size = numElements * sizeof(int);
int *h_A1 = (int*) malloc(size);

int *d_A1 = NULL;
int *d_A2 = NULL;

int normal = 0;
int medicines = 0;
int cancer = 0;

static int _s_n8 = 8;

//assigning the thread display function
pthread_t _t8;


void display();
void initialize();
void launch_main_kernel();
void launch_display_kernel();


typedef struct {
int width;
int height;
int* elements;
} Matrix;

/**
Desc : Makes one move radialy of all medicine cells recently injected
@param1 : First a vector of integer arg
*/

void MedicineGrow(vector<int> m_medicines){
for(int i = 0; i != m_medicines.size(); ++i){
if (m_medicines[i] > 1023){
h_A1[m_medicines[i]-1024+1]=10;
h_A1[m_medicines[i]-1024-1]=10;
h_A1[m_medicines[i]-1024]=10;
}
if (m_medicines[i] < 767*1023){
h_A1[m_medicines[i]+1024]=10;
if((m_medicines[i]+1)%1024 != 0) {
h_A1[m_medicines[i] + 1024 + 1] = 10;
}
if((m_medicines[i])%1024 != 0){
h_A1[m_medicines[i]+1024-1]=10;
}
}

if((m_medicines[i] + 1)%1024 != 0){
h_A1[m_medicines[i]+1]=10;
}
if((m_medicines[i])%1024 != 0) {
h_A1[m_medicines[i]-1]=10;
}
}
}

/**
Desc : Asks users for the coordinates and amount of medicine cells to be injected, and makes the correct distribution of medicines
and taken care of special cases of that distribution
*/

void* MedicineCellsInjection(void *p) {

vector<int> _medicines;

cout << "Insert the point of injection separated by space" << endl;
int _x, _y;
cin >> _x;
cin >> _y;
cout << "Insert the number of medicine cells" << endl;
int _quantity;
cin >> _quantity;


g_flag = false;

if (h_A1[_y * 1024 + _x] != 1){
h_A1[_y * 1024 + _x] = 10;
_medicines.push_back(_y * 1024 + _x);
}else  h_A1[_y * 1024 + _x] = 3; //else modify to normal cell

int _const_y = 0;
int _const_x = 0;
int _temp_y = 0;
int _temp_x = 0;

--_quantity;

_const_y = _const_y - 1024;
_temp_y = _y * 1024 + _const_y;
_temp_x = _x + _const_x;

while ((_temp_y + _temp_x) < 0) {
_const_y = _const_y + 1024;
_temp_y = _y * 1024 + _const_y;
}

while (_quantity > 0) {

//going from up to right
while ((_temp_y != _y * 1024 && (_temp_y + _temp_x) >= 0 && _quantity >= 0)) {
if (h_A1[_temp_y + _temp_x] != 1 && h_A1[_temp_y + _temp_x] != 10) {
h_A1[_temp_y + _temp_x] = 10; //input medicine
_medicines.push_back(_temp_y + _temp_x);
--_quantity;
} else {
if(h_A1[_temp_y + _temp_x] != 10){
h_A1[_temp_y + _temp_x] = 3;
--_quantity;
}
}

_const_y = _const_y + 1024;
_const_x = _const_x + 1;
_temp_y = _y * 1024 + _const_y;
_temp_x = _x + _const_x;

while ((_temp_y + _temp_x) % 1024 == 0) {
_const_x = _const_x - 1;
_temp_x = _x + _const_x;
}
}

while ((h_A1[_temp_y + _temp_x] == 10 || h_A1[_temp_y + _temp_x] == 3)
&&((_temp_y + _temp_x +1) % 1024 != 0)) {
_const_x = _const_x + 1;
_temp_x = _x + _const_x;
}

while ((h_A1[_temp_y + _temp_x] == 10 || h_A1[_temp_y + _temp_x] == 3) && (_temp_y + _temp_x +1) < 1024 * 768) {
_const_y = _const_y + 1024;
_temp_y = _y * 1024 + _const_y;
}

//going right and down
while (_temp_x != _x && _quantity >= 0 && (_temp_y + _temp_x +1) % 1024 != 0 && _quantity >= 0) {
if (h_A1[_temp_y + _temp_x] != 1 && h_A1[_temp_y + _temp_x] != 10) {
h_A1[_temp_y + _temp_x] = 10; //input medicine
_medicines.push_back(_temp_y + _temp_x);
--_quantity;
} else {
if (h_A1[_temp_y + _temp_x] != 10) {
h_A1[_temp_y + _temp_x] = 3;
--_quantity;
}
}

_const_y = _const_y + 1024;
_const_x = _const_x - 1;
_temp_y = _y * 1024 + _const_y;
_temp_x = _x + _const_x;

while ((_temp_y + _temp_x +1) > 1024 * 768) {
_const_y = _const_y - 1024;
_temp_y = _y * 1024 + _const_y;
}
}

while ((h_A1[_temp_y + _temp_x] == 10 || h_A1[_temp_y + _temp_x] == 3)  && (_temp_y + _temp_x) % 1024 != 0) {
_const_x = _const_x - 1;
_temp_x = _x + _const_x;
}

//going down and left
while (_temp_y != _y * 1024 && _quantity >= 0 && (_temp_y + _temp_x) <= 1024 * 768) {
if (h_A1[_temp_y + _temp_x] != 1 && h_A1[_temp_y + _temp_x] != 10) {
h_A1[_temp_y + _temp_x] = 10; //input medicine
_medicines.push_back(_temp_y + _temp_x);
--_quantity;
} else {
if(h_A1[_temp_y + _temp_x] != 10){
h_A1[_temp_y + _temp_x] = 3;
--_quantity;
}
}

_const_y = _const_y - 1024;
_const_x = _const_x - 1;
_temp_y = _y * 1024 + _const_y;
_temp_x = _x + _const_x;

while ((_temp_y + _temp_x +1) % 1024 == 0) {
_const_x = _const_x + 1;
_temp_x = _x + _const_x;
}
}

while((h_A1[_temp_y + _temp_x] == 10 || h_A1[_temp_y + _temp_x] == 3) && (_temp_y + _temp_x)%1024 != 0){
_const_x = _const_x - 1;
_temp_x = _x + _const_x;
}

while ((h_A1[_temp_y + _temp_x] == 10 || h_A1[_temp_y + _temp_x] == 3) & _temp_y + _temp_x & (_temp_y + _temp_x) > 0) {
_const_y = _const_y - 1024;
_temp_y = _y * 1024 + _const_y;
}

//going left and up
while (_temp_x != _x && _quantity >= 0 && (_temp_y + _temp_x) % 1024 != 0 && _quantity >= 0) {
if (h_A1[_temp_y + _temp_x] != 1 && h_A1[_temp_y + _temp_x] != 10) {
h_A1[_temp_y + _temp_x] = 10; //input medicine
_medicines.push_back(_temp_y + _temp_x);
--_quantity;
} else {
if(h_A1[_temp_y + _temp_x] != 10){
h_A1[_temp_y + _temp_x] = 3;
--_quantity;
}
}

_const_y = _const_y - 1024;
_const_x = _const_x + 1;
_temp_y = _y * 1024 + _const_y;
_temp_x = _x + _const_x;

while ((_temp_y + _temp_x) < 0) {
_const_y = _const_y + 1024;
_temp_y = _y * 1024 + _const_y;
}

}

while ((h_A1[_temp_y + _temp_x] == 10 || h_A1[_temp_y + _temp_x] == 3)  && (_temp_y -1024 + _temp_x) > 0){
_const_y = _const_y - 1024;
_temp_y = _y * 1024 + _const_y;
}
}
for(int i =0; i != 1024*768; ++i){
if (h_A1[i]==3) h_A1[i]=0;
}

sleep(g_wait_time);
MedicineGrow(_medicines);

return 0;
}


/**
Desc : Method distributed between threads to handle the cancer behaviour and also the future expansion of medicine cells if it is the case
@param1 : First void pointer
*/

__device__ void  iterating(int i, int *g_pixels, int *g_pixels2) {
int surroundingCells=0;


surroundingCells = g_pixels[i - 1024 - 1] +
g_pixels[i - 1024] +
g_pixels[i - 1024 + 1] +
g_pixels[i - 1] +
g_pixels[i + 1] +
g_pixels[i + 1024 - 1] +
g_pixels[i + 1024] +
g_pixels[i + 1024 + 1];

//checking and if it is the case modify the cancer cell
//it is cured into a healthy cell and all the surrounding medicine cells also
// become healthy cells
if (g_pixels[i] == 1) {
if ((surroundingCells / 10) >= 6){
//--g_total_cancer;
//++g_total_normal;
g_pixels2[i] = 0;
if (g_pixels[i - 1024 - 1] == 10) g_pixels2[i - 1024 - 1] = 0;
if (g_pixels[i - 1024] == 10) g_pixels2[i - 1024] = 0;
if (g_pixels[i - 1024 + 1] == 10) g_pixels2[i - 1024 + 1] = 0;
if (g_pixels[i - 1] == 10) g_pixels2[i - 1] = 0;
if (g_pixels[i + 1] == 10) g_pixels2[i + 1] = 0;
if (g_pixels[i + 1024 - 1] == 10) g_pixels2[i + 1024 - 1] = 0;
if (g_pixels[i + 1024] == 10) g_pixels2[i + 1024] = 0;
if (g_pixels[i + 1024 + 1] == 10) g_pixels2[i + 1024 + 1] = 0;

//g_total_normal = g_total_normal + surroundingCells / 10;
}
}

// if not cancer cell then it is normal or medicine cell
if (g_pixels[i] != 1) {
if ((surroundingCells % 10) >= 6) {
//if(*(g_pixels + i) == 0) --g_total_normal;
g_pixels2[i] = 1;
//++g_total_cancer;
}
}}


/**
Desc : Outputing each 1/30 seconds statistics about all cells, runned by one thread
and taken care of special cases of that distribution
@param1 : First void pointer
*/
void* displayData(void *p){
normal = 0;
cancer = 0;
medicines = 0;
for(int i = 0; i!= numElements; ++i){
if(h_A1[i] == 0) ++normal;
if(h_A1[i] == 1) ++cancer;
if(h_A1[i] == 10) ++medicines;

}
++g_iteration_counter;
if(g_flag == false) {
cout << "===================================================="
<< endl << "After iteration " << g_iteration_counter
<< endl << "Total number of cancer cells is " << cancer
<< endl << "Total number of normal cells is " << normal
<< endl << "Total number of medicine cells is " << medicines << endl
<< "====================================================" << endl;
}
return 0;
}

/**
Desc : IO call for 3 cases: exit, zoom, and insert medicines
@param1: key of pressed button
@param: x- mouse position
@param: y- mouse position
*/

void keyboard ( unsigned char key, int mousePositionX, int mousePositionY)
{
switch ( key )
{
case 27:
exit ( 0 );

break;

case 32:
flag = GL_QUADS;
width = width/6;
height = height/6;

case 9:
//assigning the thread to medicine injection
static int _s_n7 = 7;

g_flag = true;

//assigning the thread to medicine injection
pthread_t _t7;
pthread_create(&_t7, 0, &MedicineCellsInjection, &_s_n7);
pthread_kill(_t7, 0);
}
}



/**
Desc : device function for Getting a matrix element
@param: the matrix from where to get element
@param: row - the matrix row
@param: col - the column for matrix
*/

__device__ float GetElement(const Matrix A, int row, int col) {
return A.elements[row * 1024 + col];
}

/**
Desc : device function for Setting a matrix element
@param: the matrix where to set element
@param: row - the matrix row
@param: col - the column for matrix
@param: value - the value to be set
*/
__device__ void SetElement(Matrix A, int row, int col, float value) {
A.elements[row * 1024 + col] = value;
}


/**
Desc : device function for the BLOCK_SIZExBLOCK_SIZE sub-matrix Asub of A that is
located col sub-matrices to the right and row sub-matrices down from the upper-left corner of A
@param: the matrix from where to get element
@param: row - the matrix row
@param: col - the column for matrix
*/

__device__ Matrix GetSubMatrix(int *A, int row, int col) {
Matrix Asub;
Asub.width = BLOCK_SIZE;
Asub.height = BLOCK_SIZE;
Asub.elements = &A[1024* BLOCK_SIZE * row + BLOCK_SIZE * col];
return Asub;
}



/**
Desc : device function Copying back to original matrix the new values of the grid
@param: the value of thread/position to copy
@param: the matrix where to copy element
@param: the matrix from where to copy element
*/
__device__ void VectorChangeTask(int i, int *g_pixels, int *g_pixels2) {
g_pixels[i] = g_pixels2[i];
}


/**
Desc : global function to calculate the grid modifications
@param: the first matrix used to work on
@param: the second matrix where to register new modifications
@param: number of elements
*/
__global__ void
CalculationCancerGrow(int *g_pixels, int *g_pixels2, int numElements){

// Block row and column
int blockRow = blockIdx.y;
int blockCol = blockIdx.x;

// Thread row and column within Csub
int row = threadIdx.y;
int col = threadIdx.x;


for (int m = 0; m < (1024 / BLOCK_SIZE); ++m) {
// Get sub-matrix Asub of A
Matrix Asub = GetSubMatrix(g_pixels, blockRow, m);

// Shared memory used to store Asub and Bsub respectively
__shared__ int As[BLOCK_SIZE][BLOCK_SIZE];
__shared__ int Bs[BLOCK_SIZE][BLOCK_SIZE];

// Load Asub and Bsub from device memory to shared memory
// Each thread loads one element of each sub-matrix
As[row][col] = GetElement(Asub, row, col);
Bs[row][col] = GetElement(Bsub, row, col);

// Synchronize to make sure the sub-matrices are loaded
// before starting the computation
__syncthreads();


int surroundingCells=0;
if(row != 0 && row != 15 && col != 0 && col != 15){
surroundingCells = As[row][col-1] +
As[row-1][col-1] +
As[row-1][col] +
As[row-1][col+1] +
As[row][col+1] +
As[row+1][col+1] +
As[row+1][col] +
As[row+1][col-1];


//checking and if it is the case modify the cancer cell
//it is cured into a healthy cell and all the surrounding medicine cells also
// become healthy cells
if (As[row][col] == 1) {
if ((surroundingCells / 10) >= 6){
Bs[row][col] = 0;

if (As[row][col-1] == 10) Bs[row][col-1] = 0;
if (As[row-1][col-1] == 10) Bs[row-1][col-1]= 0;
if (As[row-1][col] == 10) Bs[row-1][col] = 0;
if (As[row-1][col+1] == 10) Bs[row-1][col+1] = 0;
if (As[row][col+1] == 10) Bs[row][col+1] = 0;
if (As[row+1][col+1] == 10) Bs[row+1][col+1] = 0;
if (As[row+1][col]  == 10) Bs[row+1][col] = 0;
if (As[row+1][col-1] == 10) Bs[row+1][col-1] = 0;

}
}

// if not cancer cell then it is normal or medicine cell
if (As[row][col] != 1) {
if ((surroundingCells % 10) >= 6) {
Bs[row][col] = 1;
}
}
}

__syncthreads();
g_pixels2[blockCol*col*1024 + blockRow*row] = Bs[row*blockRow][col*blockCol];

int i = blockDim.x * blockIdx.x + threadIdx.x;
VectorChangeTask(i, g_pixels, g_pixels2);
}



int main(int argc, char **argv){

glutInit(&argc, argv);

for(int i = 0; i != numElements; ++i){
h_A1[i] = 0;
}

//preparing the field
int _cancer = 1024*768*g_cancerCellsRate/100;

//random distribution of cancer cells
for (int i = 0; i!= _cancer; ++i){
int _temp = rand()%(1024*768);
while(h_A1[_temp] == 1){
_temp = rand()%(1024*768);
}
h_A1[_temp] = 1;
}

glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
glutInitWindowSize(width, height);
glutCreateWindow("Cells' grid");
glutDisplayFunc(display);
glutKeyboardFunc(keyboard);
glutIdleFunc(display);

initialize();

glutMainLoop();

return 0;
}


/**
Desc : function to run the kernel with needed parameters
*/
void launch_main_kernel(){
int threadsPerBlock = 256;
int blocksPerGrid =(1024*768 + threadsPerBlock - 1) / threadsPerBlock;

CalculationCancerGrow<<<blocksPerGrid, threadsPerBlock>>>(d_A1, d_A2, numElements);
}

/**
Desc : preparing device array by alocating needed memory and the creation of pthread for data statistic output
*/
void initializing_array(){

cudaMalloc((void **)&d_A1, size);
cudaMalloc((void **)&d_A2, size);

cudaMemcpy(d_A1, h_A1, size, cudaMemcpyHostToDevice);
cudaMemcpy(d_A2, h_A1, size, cudaMemcpyHostToDevice);

pthread_create(&_t8, 0, &displayData, &_s_n8);
}

/**
Desc : freeing the alocated memory
*/

void cleanup(){
cudaFree(d_A1);
cudaFree(d_A2);
pthread_kill(_t8, 0);
}


/**
Desc : openGl part responsable to display the grid and running the kernel calculation at each iteration of glutMainLoop();
*/

void display(){
glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT*/);

glLoadIdentity();
glMatrixMode(GL_PROJECTION);
glLoadIdentity();


gluOrtho2D(0, width, height, 0);

glClearColor(1, 1, 1,1);
glClear(GL_COLOR_BUFFER_BIT);

initializing_array();

launch_main_kernel();

cudaMemcpy(h_A1, d_A2, size, cudaMemcpyDeviceToHost);

glBegin(flag); //points/quads
for(int x = 0; x < 1024; x++)
{
for(int y = 0; y < 768; y++)
{
if (h_A1[y*1024+x]==0){
glColor3f(0, 1, 0);
glVertex2f(x, y);

if(flag== GL_QUADS) {
glVertex2f(x + 1, y);
glVertex2f(x + 1, y + 1);
glVertex2f(x, y + 1);
}
}

if (h_A1[y*1024+x]==1){
glColor3f(1, 0, 0);
glVertex2f(x, y);
if(flag== GL_QUADS) {
glVertex2f(x + 1, y);
glVertex2f(x + 1, y + 1);
glVertex2f(x, y + 1);
}
}

if (h_A1[y*1024+x]==10) {
glColor3f(1, 1, 0);
glVertex2f(x, y);
if(flag== GL_QUADS) {
glVertex2f(x + 1, y);
glVertex2f(x + 1, y + 1);
glVertex2f(x, y + 1);
}
}
}
}
sleep(g_wait_time);
glEnd();

cleanup();

glutSwapBuffers();
}

/**
Desc : openGl initialization part
*/
void initialize ()
{
glMatrixMode(GL_PROJECTION);
glViewport(0, 0, width, height);
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
GLfloat aspect = (GLfloat) width / height;
gluPerspective(45, aspect, 0.1f, 10.0f);
glClearColor(0.0, 0.0, 0.0, 0.0);
}


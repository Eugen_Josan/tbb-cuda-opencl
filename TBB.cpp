#include <iostream>
#include <string>

#include <stdlib.h>
#include <bits/stl_algo.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <unistd.h>

#include <math.h>
#include <iomanip>


#include <tbb/tbb.h>
#include <tbb/parallel_for.h>
#include "tbb/concurrent_vector.h"
#include "tbb/compat/thread"
#include "tbb/atomic.h"


using namespace std;

using namespace tbb;


// Initial size of graphics window on your screen.
const int WIDTH  = 1024; // in pixels
const int HEIGHT = 768; //


// Current size of window (will change when you resize window)
int width  = WIDTH;
int height = HEIGHT;

GLenum flag = GL_POINTS;

const int g_cancerCellsRate = 50;
const short normal_cells = 0;

double g_wait_time = 2;

atomic<int> g_total_cancer = 1024*768*g_cancerCellsRate/100;
atomic<int> g_total_normal = 1024*768 - g_total_cancer;
atomic<int> g_iteration_counter = 0;

bool g_flag = false;

concurrent_vector<short> *g_pixels = new concurrent_vector<short>(1024*768, normal_cells);
concurrent_vector<short> *g_pixels2 = new concurrent_vector<short>(1024*768, normal_cells);

//OpenGL part used from Tutorials
void display()
{
    glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT*/);

    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();


    gluOrtho2D(0, width, height, 0);

    glClearColor(1, 1, 1,1);
    glClear(GL_COLOR_BUFFER_BIT);

    glBegin(flag); //points/quads
    for(int x = 0; x < 1024; x++)
    {
        for(int y = 0; y < 768; y++)
        {
            if (g_pixels2->at(y*1024+x)==0){
                glColor3f(0, 1, 0);
                glVertex2f(x, y);

                if(flag== GL_QUADS) {
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                }
            }

            if (g_pixels2->at(y*1024+x)==1){
                glColor3f(1, 0, 0);
                glVertex2f(x, y);
                if(flag== GL_QUADS) {
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                }
            }

            if (g_pixels2->at(y*1024+x)==10) {
                glColor3f(1, 1, 0);
                glVertex2f(x, y);
                if(flag== GL_QUADS) {
                    glVertex2f(x + 1, y);
                    glVertex2f(x + 1, y + 1);
                    glVertex2f(x, y + 1);
                }
            }
        }
    }
    glEnd();

    glutSwapBuffers();
}


//OpenGl
void initialize ()
{
    glMatrixMode(GL_PROJECTION);
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    GLfloat aspect = (GLfloat) width / height;
    gluPerspective(45, aspect, 0.1f, 10.0f);
    glClearColor(0.0, 0.0, 0.0, 0.0);
}


/**
Desc : Makes one move radialy of all medicine cells recently injected
@param1 : First a vector of integer arg
*/

void MedicineGrow(vector<int> m_medicines){
    for(int i = 0; i != m_medicines.size(); ++i){
        if (m_medicines[i] > 1023){
            g_pixels2->at(m_medicines[i]-1024+1)=10;
            g_pixels2->at(m_medicines[i]-1024-1)=10;
            g_pixels2->at(m_medicines[i]-1024)=10;
            g_total_normal = g_total_normal - 3;
        }
        if (m_medicines[i] < 767*1023){
            g_pixels2->at(m_medicines[i]+1024)=10;
            --g_total_normal;
            if((m_medicines[i]+1)%1024 != 0) {
                --g_total_normal;
                g_pixels2->at(m_medicines[i] + 1024 + 1) = 10;
            }
            if((m_medicines[i])%1024 != 0){
                --g_total_normal;
                g_pixels2->at(m_medicines[i]+1024-1)=10;
            }
        }

        if((m_medicines[i]+1)%1024 != 0){
            --g_total_normal;
            g_pixels2->at(m_medicines[i]+1)=10;
        }
        if((m_medicines[i])%1024 != 0) {
            --g_total_normal;
            g_pixels2->at(m_medicines[i]-1)=10;
        }
    }
}

/**
Desc : Asks users for the coordinates and amount of medicine cells to be injected, and makes the correct distribution of medicines
and taken care of special cases of that distribution
@param1 :
*/

void MedicineCellsInjection() {

    vector<int> _medicines;

    cout << "Insert the point of injection separated by space" << endl;
    int _x, _y;
    cin >> _x;
    cin >> _y;
    cout << "Insert the number of medicine cells" << endl;
    int _quantity;
    cin >> _quantity;


    g_flag = false;

    if (g_pixels2->at(_y * 1024 + _x) != 1){
        g_pixels2->at(_y * 1024 + _x) = 10;
        _medicines.push_back(_y * 1024 + _x);
    }else  g_pixels2->at(_y * 1024 + _x) = 3; //else modify to normal cell

    int _const_y = 0;
    int _const_x = 0;
    int _temp_y = 0;
    int _temp_x = 0;

    --_quantity;

    _const_y = _const_y - 1024;
    _temp_y = _y * 1024 + _const_y;
    _temp_x = _x + _const_x;

    while ((_temp_y + _temp_x) < 0) {
        _const_y = _const_y + 1024;
        _temp_y = _y * 1024 + _const_y;
    }

    while (_quantity > 0) {

        //going from up to right
        while ((_temp_y != _y * 1024 && (_temp_y + _temp_x) >= 0 && _quantity >= 0)) {
            if (g_pixels2->at(_temp_y + _temp_x) != 1 && g_pixels2->at(_temp_y + _temp_x) != 10) {
                g_pixels2->at(_temp_y + _temp_x) = 10; //input medicine
                _medicines.push_back(_temp_y + _temp_x);
                --_quantity;
            } else {
                if(g_pixels2->at(_temp_y + _temp_x) != 10){
                    g_pixels2->at(_temp_y + _temp_x) = 3;
                    --_quantity;
                }
            }

            _const_y = _const_y + 1024;
            _const_x = _const_x + 1;
            _temp_y = _y * 1024 + _const_y;
            _temp_x = _x + _const_x;

            while ((_temp_y + _temp_x) % 1024 == 0) {
                _const_x = _const_x - 1;
                _temp_x = _x + _const_x;
            }
        }

        while ((g_pixels2->at(_temp_y + _temp_x) == 10 || g_pixels2->at(_temp_y + _temp_x) == 3)
                &&((_temp_y + _temp_x +1) % 1024 != 0)) {
            _const_x = _const_x + 1;
            _temp_x = _x + _const_x;
        }

        while ((g_pixels2->at(_temp_y + _temp_x) == 10 || g_pixels2->at(_temp_y + _temp_x) == 3) && (_temp_y + _temp_x +1) < 1024 * 768) {
            _const_y = _const_y + 1024;
            _temp_y = _y * 1024 + _const_y;
        }

            //going right and down
            while (_temp_x != _x && _quantity >= 0 && (_temp_y + _temp_x +1) % 1024 != 0 && _quantity >= 0) {
                if (g_pixels2->at(_temp_y + _temp_x) != 1 && g_pixels2->at(_temp_y + _temp_x) != 10) {
                    g_pixels2->at(_temp_y + _temp_x) = 10; //input medicine
                    _medicines.push_back(_temp_y + _temp_x);
                    --_quantity;
                } else {
                    if (g_pixels2->at(_temp_y + _temp_x) != 10) {
                        g_pixels2->at(_temp_y + _temp_x) = 3;
                        --_quantity;
                    }
                }

                _const_y = _const_y + 1024;
                _const_x = _const_x - 1;
                _temp_y = _y * 1024 + _const_y;
                _temp_x = _x + _const_x;

                while ((_temp_y + _temp_x +1) > 1024 * 768) {
                    _const_y = _const_y - 1024;
                    _temp_y = _y * 1024 + _const_y;
                }
            }

        while ((g_pixels2->at(_temp_y + _temp_x) == 10 || g_pixels2->at(_temp_y + _temp_x) == 3)  && (_temp_y + _temp_x) % 1024 != 0) {
            _const_x = _const_x - 1;
            _temp_x = _x + _const_x;
        }

        //going down and left
        while (_temp_y != _y * 1024 && _quantity >= 0 && (_temp_y + _temp_x) <= 1024 * 768) {
            if (g_pixels2->at(_temp_y + _temp_x) != 1 && g_pixels2->at(_temp_y + _temp_x) != 10) {
                g_pixels2->at(_temp_y + _temp_x) = 10; //input medicine
                _medicines.push_back(_temp_y + _temp_x);
                --_quantity;
            } else {
                if(g_pixels2->at(_temp_y + _temp_x) != 10){
                    g_pixels2->at(_temp_y + _temp_x) = 3;
                    --_quantity;
                }
            }

            _const_y = _const_y - 1024;
            _const_x = _const_x - 1;
            _temp_y = _y * 1024 + _const_y;
            _temp_x = _x + _const_x;

            while ((_temp_y + _temp_x +1) % 1024 == 0) {
                _const_x = _const_x + 1;
                _temp_x = _x + _const_x;
            }
        }

        while((g_pixels2->at(_temp_y + _temp_x) == 10 || g_pixels2->at(_temp_y + _temp_x) == 3) && (_temp_y + _temp_x)%1024 != 0){
            _const_x = _const_x - 1;
            _temp_x = _x + _const_x;
        }

            while ((g_pixels2->at(_temp_y + _temp_x) == 10 || g_pixels2->at(_temp_y + _temp_x) == 3) & _temp_y + _temp_x & (_temp_y + _temp_x) > 0) {
            _const_y = _const_y - 1024;
            _temp_y = _y * 1024 + _const_y;
        }

        //going left and up
        while (_temp_x != _x && _quantity >= 0 && (_temp_y + _temp_x) % 1024 != 0 && _quantity >= 0) {
            if (g_pixels2->at(_temp_y + _temp_x) != 1 && g_pixels2->at(_temp_y + _temp_x) != 10) {
                g_pixels2->at(_temp_y + _temp_x) = 10; //input medicine
                _medicines.push_back(_temp_y + _temp_x);
                --_quantity;
            } else {
                if(g_pixels2->at(_temp_y + _temp_x) != 10){
                    g_pixels2->at(_temp_y + _temp_x) = 3;
                    --_quantity;
                }
            }

            _const_y = _const_y - 1024;
            _const_x = _const_x + 1;
            _temp_y = _y * 1024 + _const_y;
            _temp_x = _x + _const_x;

            while ((_temp_y + _temp_x) < 0) {
                _const_y = _const_y + 1024;
                _temp_y = _y * 1024 + _const_y;
            }

        }

        while ((g_pixels2->at(_temp_y + _temp_x) == 10 || g_pixels2->at(_temp_y + _temp_x) == 3)  && (_temp_y -1024 + _temp_x) > 0){
            _const_y = _const_y - 1024;
            _temp_y = _y * 1024 + _const_y;
        }
    }
    for(int i =0; i != g_pixels2->size(); ++i){
        if (g_pixels2->at(i)==3) g_pixels2->at(i)=0;
    }

    g_total_normal = g_total_normal - _medicines.size();
    //small delay to allow to see the changes
    sleep(g_wait_time);
    MedicineGrow(_medicines);

}


/**
Desc : Method distributed between threads to handle the cancer behaviour and also the future expansion of medicine cells if it is the case
@param1 : First void pointer
*/

void iterating(int i) {
    int surroundingCells=0;
    int _temp = 0;

                surroundingCells = g_pixels->at(i - 1024 - 1) +
                        g_pixels->at(i - 1024) +
                        g_pixels->at(i - 1024 + 1) +
                        g_pixels->at(i - 1) +
                        g_pixels->at(i + 1) +
                        g_pixels->at(i + 1024 - 1) +
                        g_pixels->at(i + 1024) +
                        g_pixels->at(i + 1024 + 1);

                //checking and if it is the case modify the cancer cell
                //it is cured into a healthy cell and all the surrounding medicine cells also
                // become healthy cells
    if (g_pixels->at(i) == 1) {
                if ((surroundingCells / 10) >= 6) {
                    --g_total_cancer;
                    ++g_total_normal;
                    g_pixels2->at(i) = 0;
                    if (g_pixels->at(i - 1024 - 1) == 10) g_pixels2->at(i - 1024 - 1) = 0;
                    if (g_pixels->at(i - 1024) == 10) g_pixels2->at(i - 1024) = 0;
                    if (g_pixels->at(i - 1024 + 1) == 10) g_pixels2->at(i - 1024 + 1) = 0;
                    if (g_pixels->at(i - 1) == 10) g_pixels2->at(i - 1) = 0;
                    if (g_pixels->at(i + 1) == 10) g_pixels2->at(i + 1) = 0;
                    if (g_pixels->at(i + 1024 - 1) == 10) g_pixels2->at(i + 1024 - 1) = 0;
                    if (g_pixels->at(i + 1024) == 10) g_pixels2->at(i + 1024) = 0;
                    if (g_pixels->at(i + 1024 + 1) == 10) g_pixels2->at(i + 1024 + 1) = 0;

                    g_total_normal = g_total_normal + surroundingCells / 10;
                }
    }

        // if not cancer cell then it is normal or medicine cell
            if (g_pixels->at(i) != 1) {
                if ((surroundingCells % 10) >= 6) {
                    if(g_pixels->at(i) == 0) --g_total_normal;
                    g_pixels2->at(i) = 1;
                    ++g_total_cancer;
                }
            }
}


/**
Desc : Outputing each 1/30 seconds statistics about all cells, runned by one thread
and taken care of special cases of that distribution
@param1 : First void pointer
*/
void* DisplayStatistics(){
    ++g_iteration_counter;

    if(g_flag == false) {
        int _medicines = 1024*768 - g_total_cancer - g_total_normal;
        cout << "===================================================="
                << endl << "After iteration " << g_iteration_counter
                << endl << "Total number of cancer cells is " << g_total_cancer
                << endl << "Total number of normal cells is " << g_total_normal
                << endl << "Total number of medicine cells is " << _medicines << endl
                << "====================================================" << endl;
    }
}

class medicine_task {
public:
    void operator()() {
        MedicineCellsInjection();
    }
};

//part of Opengl
void keyboard ( unsigned char key, int mousePositionX, int mousePositionY )
{
    switch ( key )
    {
        case 27:
            exit ( 0 );

            break;

        case 32:
            flag = GL_QUADS;
            width = width/6;
            height = height/6;

        case 9:
//        //assigning the thread to medicine injection
            g_flag = true;
            medicine_task t1;
            tbb_thread medicineThread(t1);
            medicineThread.~tbb_thread_v3();

    }
}

class OpenGL
{
public:
    void operator()()
    {
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
        glutInitWindowSize(width, height);
        glutCreateWindow("Cells' grid");
        glutDisplayFunc(display);
        glutKeyboardFunc( keyboard );
        glutIdleFunc( display );

        initialize();

        glutMainLoop();

    }
};


class CalculationsCancerGrow{
public:

    void operator()( const blocked_range<int>& r ) const {
        for (int i=r.begin(); i!=r.end(); ++i ) {
            iterating(i);
        }
    }
};


void ParallelApplyCalculation() {
    static affinity_partitioner ap;
    parallel_for(blocked_range<int>(0 + 1025, 1024 * 768 - 1024 - 1), CalculationsCancerGrow(), ap);
}


void vectorChangeTask() {
    parallel_for( blocked_range<int>(0,1024*768),
            [=](const blocked_range<int>& r) {
                for(size_t i=r.begin(); i!=r.end(); ++i)
                    g_pixels->at(i) = g_pixels2->at(i);
            }
    );
}

class vectorChange_task : public task {
public:
    task *execute() {
        vectorChangeTask();

        return NULL;
    }
};

class store_task : public task {
public:
    task *execute() {
        DisplayStatistics();
        return NULL;
    }
};

class loop_task : public task {
public:
    task *execute() {
        ParallelApplyCalculation();
        return NULL;
    }
};


class main_task : public task {
public:
    task* execute( ) {

        loop_task& a = *new( allocate_child() ) loop_task( );
        main_task & f = *new( allocate_child()) main_task( );

        set_ref_count(2);

        spawn(a);
        wait_for_all();


        task_list non_dependent_tasks;
        non_dependent_tasks.push_back(*new( allocate_child() ) store_task( ));
        non_dependent_tasks.push_back(*new( allocate_child() ) vectorChange_task( ));

        set_ref_count(3);

        spawn_and_wait_for_all(non_dependent_tasks);

        increment_ref_count();
        recycle_as_safe_continuation();

        sleep(g_wait_time);

        return &f;
    }
};


int main(int argc, char **argv){
   task_scheduler_init init;
    glutInit(&argc, argv);

    //preparing the field
    int _cancer = 1024*768*g_cancerCellsRate/100;

    //random distribution of cancer cells
    for (int i = 0; i!= _cancer; ++i){
        int _temp = rand()%(1024*768);
        while(g_pixels->at(_temp) == 1){
            _temp = rand()%(1024*768);
        }
        g_pixels->at(_temp) = 1;
        g_pixels2->at(_temp) = 1;
    }

    OpenGL t1;
    tbb_thread openGLThread(t1);
    
    main_task &f1 = *new(task::allocate_root()) main_task();
    task::spawn_root_and_wait(f1);

    return 0;
}
